stuttgart-things/install-configure-podman
=========================================

role for installing podman, buildah and skopeo on various os.

## Step 1: Install ansible requirements

copy and paste the following into your terminal:

```
cat <<EOF > /tmp/requirements.yaml
- src: git@codehub.sva.de:Lab/stuttgart-things/supporting-roles/install-configure-podman.git
  version: stable
  scm: git
- src: git@codehub.sva.de:Lab/stuttgart-things/supporting-roles/install-requirements.git
  version: stable
  scm: git
EOF
ansible-galaxy install -r /tmp/requirements.yaml --force
rm -rf /tmp/requirements.yaml
```

## Step 2: define & run playbook 

copy and paste the following (on any place of the filesystem of the ansible host) into your terminal:

```
cat <<EOF > install-configure-podman.yaml
---
- hosts: "{{ target_host }}"
  become: true
  roles:
    - role: install-configure-podman
EOF 
```

Execute playbook:
```
ansible-playbook -i my_inventory install-configure-podman.yaml -vv 
```

License
-------

BSD

Author Information
------------------

Patrick Hermann (phermann1988@gmail.com); 04/2020